from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from config import urlsql
import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = urlsql
SQLALCHEMY_TRACK_MODIFICATIONS = True
db = SQLAlchemy(app)
db.init_app(app)

class Tasking(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.CHAR())
    description = db.Column(db.CHAR())
    created_at = db.Column(db.Date)
    due_date = db.Column(db.Date)
    is_done = db.Column(db.Boolean)

    def __init__(self, title, description, created_at, due_date, is_done):
        self.title = title
        self.description = description
        self.created_at = created_at
        self.due_date = due_date
        self.is_done = is_done

    def __repr__(self):

        return "[%s,%s,%s,%s,%s,%s]"%(self.id,self.title, self.description,self.created_at,self.due_date,self.is_done)



@app.route('/api/v1/task/', methods=['POST'])
def submit():
    tasks = []
    try:
        title = request.json['title']
        description = request.json['description']
        due_date = request.json['due_date']
        created_at = datetime.date.today()
        if due_date < str(datetime.date.today()):
            return make_response(jsonify({'error': 'Примерная дата окончания задания не может быть меньше сегодняшней!'}), 406)
        insert = Tasking(title=title, description=description, due_date=due_date, created_at=created_at, is_done=False)
        db.session.add(insert)
        db.session.commit()
        tasks.append({'id':insert.id,
                   'title': insert.title,
                   'description': insert.description,
                   'due_date': str(insert.due_date),
                   'created_at':str(insert.created_at),
                   'is_done':insert.is_done})
    except BaseException:
        return make_response(jsonify({'error':'переданы не все параметры'}), 406)

    return make_response(jsonify({'response':tasks}), 200)



@app.route('/api/v1/task/<int:id>/', methods=['GET'])
def result(id):
    try:
        res = db.session.query(Tasking).filter_by(id=id).first()
        tasks ={'id': res.id,
                'title':res.title,
                'description':res.description,
                'created_at':str(res.created_at),
                'due_date':str(res.due_date),
                'is_done':res.is_done}
        return make_response(jsonify(tasks), 200)
    except BaseException:
        return make_response(jsonify({'error':BaseException}), 406)


@app.route('/api/v1/task/<int:id>/', methods=['PATCH'])
def updated(id):

    try:
        title = request.json['title']
        description = request.json['description']
        due_date = request.json['due_date']
        is_done = request.json['is_done']
        if due_date < str(datetime.date.today()):
            return make_response(jsonify({'error': 'Примерная дата окончания задания не может быт ьменьше сегодняшней!'}), 406)
        tasks = db.session.query(Tasking).filter_by(id=id).first()
        tasks.title = title
        tasks.description = description
        tasks.due_date = due_date
        tasks.is_done=bool(is_done)
        db.session.commit()
        task_update = {'id': tasks.id,
                       'title': tasks.title,
                       'description': tasks.description,
                       'due_date': str(tasks.due_date),
                       'created_at': str(tasks.created_at),
                       'is_done': tasks.is_done}
    except BaseException:
        return make_response(jsonify({'error':BaseException}), 406)
    return make_response(jsonify(task_update),200)

@app.route('/api/v1/task/<int:id>/', methods=['DELETE'])
def delete(id):
        try:
            db.session.query(Tasking).filter_by(id=id).delete()
            db.session.commit()
            return make_response(jsonify({}),200)
        except Exception:
            return make_response(jsonify({'error':'нет такого задания'}), 406)

@app.route('/api/v1/task/', methods=['GET'])
def filtered():

    response = None
    parametr = request.args.get('param')
    is_done = request.args.get('is_done')
    overdue = request.args.get('overdue')
    created_at_start = request.args.get('created_at_start')
    created_at_end = request.args.get('created_at_end')
    due_date_start = request.args.get('due_date_start')
    due_date_end = request.args.get('due_date_end')

    query = []

    if parametr:
       response = db.session.query(Tasking).all()

    if overdue:
        response = db.session.query(Tasking).filter((Tasking.is_done == overdue) &
                                                    (Tasking.due_date < str(datetime.date.today())))
    if created_at_start and created_at_end:
        response = db.session.query(Tasking).filter((Tasking.created_at >= created_at_start) &
                                                    (Tasking.created_at <= created_at_end))
    if due_date_end and due_date_start:
        response = db.session.query(Tasking).filter((Tasking.due_date >= due_date_start) &
                                                    (Tasking.due_date <= due_date_end))
    if created_at_start and due_date_end:
        response = db.session.query(Tasking).filter((created_at_start < due_date_end))

    if is_done == 'True' or is_done == 'False' or is_done == '1' or is_done == '0' or is_done:
        response =  db.session.query(Tasking).filter((Tasking.is_done == is_done))
        if due_date_end and due_date_start:
            response = db.session.query(Tasking).filter((Tasking.is_done == is_done) &
                                                        (Tasking.due_date >= due_date_start) &
                                                        (Tasking.due_date <= due_date_end))
        if created_at_start and created_at_end:
            response = db.session.query(Tasking).filter((Tasking.is_done == is_done) &
                                                        (Tasking.created_at >= created_at_start) &
                                                        (Tasking.created_at <= created_at_end))
        if created_at_start and due_date_end:
            response = db.session.query(Tasking).filter((Tasking.is_done == is_done) &
                                                        (created_at_start < due_date_end))

    for res in response:
        query.append({'id': res.id,
                      'title': res.title,
                      'description': res.description,
                      'due_date': str(res.due_date),
                      'created_at': str(res.created_at),
                      'is_done': res.is_done})
    return make_response(jsonify(query), 200)

if __name__ == '__main__':
    app.run(debug=True)
