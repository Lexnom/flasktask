FROM python:3.7

RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev build-essential

ENV DATABASE_URL = ${DATABASE_URL}
WORKDIR ./flasktask

RUN pip install --upgrade pip
RUN pip3 install Flask==1.1.1
RUN pip3 install Flask-SQLAlchemy==2.4.0
RUN pip3 install psycopg2==2.8.3


EXPOSE 5000
ENTRYPOINT ["python3.7"]
CMD ["FlaskHello.py"]

